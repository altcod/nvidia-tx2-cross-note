# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Search nvidia tx2 cross compile how to ###

* Manually install a [Linaro toolchain](https://releases.linaro.org/components/toolchain/binaries/)
* You're probably intersted in a combination of aarch64-linux-gnu and armv8l-linux-gnueabihf (or possibly arm-linux-gnueabihf ). The 64-bit aarch64/arm64 has a compatibility mode...if you do not build kernels you will not need anything 32-bit (gnueabihf is 32-bit). I do not know what Qt will require.
* Consider cloning the rootfs partition of your Jetson if you run into complicated sysroot needs. [Cloning explained here (there is a patch listed for flash.sh)](https://bitbucket.org/tutorials/markdowndemo).
* [Reference](https://devtalk.nvidia.com/default/topic/1016886/configure-cross-compile-for-nvidia-jetson-tx2-on-ubuntu-64bit-gives-error/?offset=1).

### Linaro toolchain ###

* Available latest versions: 4.9-2017-.01, 5.5-2017.10, 6.4-2017.11, 7.2-2017.11. 
* Try: 5.5-2017.10, aarch64-linux-gnu
* Download: gcc-x86_64, runtime, sysroot-glibc

### Crosscompile tensorflow for tx2 ###

* [Wheel](https://github.com/peterlee0127/tensorflow-nvJetson)
* [Reference](https://devtalk.nvidia.com/default/topic/1026925/cross-compile-tensorflow-on-host-not-on-board-tx2-/)

### Crosscompile tensorflow for tx2 ###

* https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/makefile/README.md
* Steps to setup:
```
cd
git clone https://github.com/tensorflow/tensorflow
cd tensorflow
tensorflow/contrib/makefile/download_dependencies.sh
wget https://storage.googleapis.com/download.tensorflow.org/models/inception5h.zip
mkdir ~/graphs
mv inception5h.zip ~/graphs
unzip ~/graphs/inception5h.zip -d ~/graphs/inception
```

* Setup for native linux build: 
```
sudo apt-get install autoconf automake libtool curl make g++ unzip zlib1g-dev \
git python
```

* Steps to build:
```
cd
cd tensorflow
tensorflow/contrib/makefile/build_all_linux.sh
```
